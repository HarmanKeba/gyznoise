//
//  LoginVC.swift
//  GysNoize
//
//  Created by iMac_2 on 8/2/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import FirebaseAuth

class LoginVC: UIViewController, FBSDKLoginButtonDelegate, GIDSignInUIDelegate {
    
    //@IBOutlet weak var name: UITextField!
    //@IBOutlet weak var emailLogin: UITextField!
    //@IBOutlet weak var password: UITextField!
   // @IBOutlet weak var loginEmailBtn: UIButton!
    
    
    //let loginFBBtn = FBSDKLoginButton()
    //let loginGglBtn = GIDSignInButton()

    override func viewDidLoad() {
        super.viewDidLoad()
//        setupFBBtn()
//        setupGglBtn()
        
        
        //try! Auth.auth().signOut()
//
        
        
        
        
       // view.addSubview(loginFBBtn)
      //
        //loginFBBtn.frame = CGRect(x: loginEmailBtn.frame.minX, y: loginEmailBtn.frame.maxY + 10.0, width: loginEmailBtn.frame.width, height: loginEmailBtn.frame.height)
        //loginGglBtn.frame = loginFBBtn.frame
        //loginGglBtn.frame.origin.y = loginGglBtn.frame.maxY
        //loginFBBtn.delegate = self
        //GIDSignIn.sharedInstance().uiDelegate = self

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard Auth.auth().currentUser == nil else {
          print("TOKEN: \(Auth.auth().currentUser?.email!)")
            
//print("TOKEN  \(Auth.auth().currentUser?.providerData ?? "!!!")")
            Auth.auth().currentUser?.getIDToken(completion: { (token, err) in
               // print("TOKEN: \(token!)")
            })
            performSegue(withIdentifier: "showMain", sender: self)
            return
        }
       
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    fileprivate func setupGglBtn() {
        //let loginGglBtn = GIDSignInButton()
       // view.addSubview(loginGglBtn)
       // loginGglBtn.frame = loginFBBtn.frame
       // loginGglBtn.frame.origin.y = loginGglBtn.frame.maxY
//        loginFBBtn.delegate = self
//        loginFBBtn.readPermissions = ["email", "public_profile"]
       // GIDSignIn.sharedInstance().uiDelegate = self

        
        
    }
    
    fileprivate func setupFBBtn() {
        
        
    }
    
    @IBAction func logginPressedFbk(_ sender: Any) {
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, err) in
            if err != nil {
                print("Custom FB Login failed:", err!)
                return
            }
            self.showEmailAddress()
        }
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let dest = segue.destination
//            self.present(dest, animated: true, completion: nil)
//    }
    
//    @IBAction func logginPressedGgl(_ sender: Any) {
//        GIDSignIn.sharedInstance().signIn()
//    }
//    

//    @IBAction func logginPressedEmail(_ sender: Any) {
//        if let email = emailLogin.text, let password = password.text, email.characters.count > 0 && password.characters.count > 0 {
//            
//            AuthService.instance.login(email: email, password: password, onComplete: { (errMsg, data) in
//                guard errMsg == nil else {
//                    let alert = UIAlertController(title: "Error Authentication", message: errMsg, preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//                    return
//                }
//                self.dismiss(animated: true, completion: nil)
//            })
//            
//        } else {
//            let alert = UIAlertController(title: "Username and Password Requared", message: "You must enter both user a name and a password", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            present(alert, animated: true, completion: nil)
//        }
//    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logiOUTpressed")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
        showEmailAddress()
        print("successfully")
       
        }
    
    func showEmailAddress() {
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"]).start { (connection, result, err) in
            if err != nil {
                print("Failed to start graph request", err!)
                return
            }
            print(result!)
            
        }
    }
    }

