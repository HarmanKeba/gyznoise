//
//  ViewControllerError.swift
//  GysNoize
//
//  Created by iMac_2 on 8/8/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit

extension UIViewController {
    func showError(_ title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
    }
}


    extension NSError {
        static func createError(_ code: Int, description: String) -> NSError {
            return NSError(domain: "com.firebase", code: 400, userInfo: [ "NSLocalizedDescription" : description ])
        }
}

extension UIStoryboard {
    
    class func getRandomViewController() -> AudioPlayerVC {
        
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AudioPlayerVC") as! AudioPlayerVC
    }
    
}
