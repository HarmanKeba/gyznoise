//
//  MainVC.swift
//  GysNoize
//
//  Created by iMac_2 on 8/2/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

struct SomeData {
    var image: UIImage?
    var index: IndexPath?
    var convert: CGRect?
    var convertTriangle: CGRect?
}

class MainVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet weak var myViewHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    
    var animationController: AnimationController = AnimationController()
    
    fileprivate let imageLoadQueue = OperationQueue()
    fileprivate var imageLoadOperations = [IndexPath: ImageLoadOperation]()
    
    var data: SomeData?
    var suggestesInstanceVC: SuggestesGenresVC?
    
    lazy var myView = UIView()
    lazy var imageAudioView = UIImageView()
    lazy var artistLblPlayer = UILabel()
    lazy var titleLblPlayer = UILabel()
    lazy var playBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AudioViewModelController.share.retrieveData { [weak self] (success, error) in
            guard let strongSelf = self else { return }
            if !success {
                let title = "Error"
                if let error = error {
                    strongSelf.showError(title, message: error.localizedDescription)
                } else {
                    strongSelf.showError(title, message: NSLocalizedString("Can't retrieve contacts.", comment: "Can't retrieve contacts."))
                }
            } else {
                let x = AudioViewModelController.share.viewModel(at: 0)
                print(x!.audio)
                print("")
                let controller = UIStoryboard.getRandomViewController()
                controller.url = x?.audio
                strongSelf.performSegue(withIdentifier: "test", sender: x?.audio)
                
                
//                strongSelf.viewProgress?.stopAnimating()
//                strongSelf.viewProgress?.removeFromSuperview()
                
//                if strongSelf.reload {
//                    if let indexPath = self?.collectionView?.indexPathsForVisibleItems {
//                        strongSelf.reload = false
//                        strongSelf.collectionView.performBatchUpdates({
//                            UIView.setAnimationsEnabled(false)
//                            strongSelf.collectionView.reloadItems(at: indexPath)
//                            UIView.setAnimationsEnabled(true)
//                        }, completion: { ok in strongSelf.reload = true })
//                    }
               // } else {
                   // strongSelf.collectionView.reloadData()!!!!!!!!!!!!!!!!!!!
                   // strongSelf.reload = true
               //}
            }
        }
        
//        DataService.instance.userRef.observeSingleEvent(of: .value, with: { (snapshot) in
//            if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
//                 for snap in snapshots {
//                    if let postDict = snap.value as? Dictionary<String, AnyObject> {
//                        print(postDict)
//                    }
//                }
//                
//            }
//        }, withCancel: nil)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        if #available(iOS 10.0, *) {
            tableView?.prefetchDataSource = self
        }
        
        self.navigationItem.title = "ENJOY"
        if revealViewController() != nil {
            //            revealViewController().rearViewRevealWidth = 62
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            
            revealViewController().rearViewRevealWidth = 250
            //            extraButton.target = revealViewController()
            //            extraButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            createAudioPlayerBottom()
            
        }
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    
    func createAudioPlayerBottom() {
        self.view.addSubview(myView)
        myView.backgroundColor = UIColor(red: 220/257, green: 220/257, blue: 220/257, alpha: 0.85)
        myView.translatesAutoresizingMaskIntoConstraints = false
        myView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0.0).isActive = true
        myView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0.0).isActive = true
        myView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0.0).isActive = true
        myViewHeightConstraints = NSLayoutConstraint(item: myView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0.0)
        myViewHeightConstraints?.isActive = true
        myViewHeightConstraints.constant = 70.0
        
        self.view.bringSubview(toFront: myView)
        myView.addSubview(imageAudioView)
        imageAudioView.contentMode = .scaleAspectFill
        imageAudioView.clipsToBounds = true
        imageAudioView.translatesAutoresizingMaskIntoConstraints = false
        imageAudioView.leadingAnchor.constraint(equalTo: myView.leadingAnchor, constant: 8.0).isActive = true
        imageAudioView.centerYAnchor.constraint(equalTo: myView.centerYAnchor).isActive = true
        imageAudioView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        imageAudioView.widthAnchor.constraint(equalToConstant: 60.0).isActive = true
        imageAudioView.backgroundColor = UIColor.gray
        
        artistLblPlayer.text = "U2"
        artistLblPlayer.textColor = UIColor.black
        artistLblPlayer.font = UIFont.boldSystemFont(ofSize: 12.0)
        artistLblPlayer.translatesAutoresizingMaskIntoConstraints = false
        
        titleLblPlayer.text = "The One"
        titleLblPlayer.textColor = UIColor.black
        titleLblPlayer.font = UIFont.systemFont(ofSize: 12.0)
        titleLblPlayer.translatesAutoresizingMaskIntoConstraints = false
        
        let stackView = UIStackView()
        myView.addSubview(stackView)
        stackView.axis = UILayoutConstraintAxis.vertical
        stackView.distribution = UIStackViewDistribution.equalSpacing
        stackView.alignment = UIStackViewAlignment.center
        stackView.spacing = 9.0
        stackView.addArrangedSubview(artistLblPlayer)
        stackView.addArrangedSubview(titleLblPlayer)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.centerYAnchor.constraint(equalTo: myView.centerYAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: imageAudioView.trailingAnchor, constant: 8.0).isActive = true
        
        myView.addSubview(playBtn)
        playBtn.translatesAutoresizingMaskIntoConstraints = false
        playBtn.trailingAnchor.constraint(equalTo: myView.trailingAnchor, constant: -10.0).isActive = true
        playBtn.centerYAnchor.constraint(equalTo: myView.centerYAnchor).isActive = true
        playBtn.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        playBtn.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        playBtn.backgroundColor = UIColor.green
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //        var user = Auth.auth().currentUser
        //        user?.delete(completion: { error in
        //            print(error?.localizedDescription ?? "!!!!")
        //        })
        //        guard Auth.auth().currentUser != nil else {
        //            performSegue(withIdentifier: "LoginVC", sender: nil)
        //            return
        //        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let url = sender as? URL, let controller = segue.destination as? AudioPlayerVC else {
            
            return
        }
       controller.url = url
//        if let detail = segue.destination as? SuggestesGenresVC {
//            self.suggestesInstanceVC = detail
        }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        //return AudioViewModelController.share.viewModelsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestesTracksCell", for: indexPath) as! SuggestesTracksCell
        return cell
    }
}


extension MainVC: UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if let _ = imageLoadOperations[indexPath] { return }
            if let viewModel = AudioViewModelController.share.viewModel(at: (indexPath as NSIndexPath).row) {
                let imageLoadOperation = ImageLoadOperation(url: viewModel.images)
                imageLoadQueue.addOperation(imageLoadOperation)
                imageLoadOperations[indexPath] = imageLoadOperation
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            guard let imageLoadOperation = imageLoadOperations[indexPath] else { return }
            imageLoadOperation.cancel()
            imageLoadOperations.removeValue(forKey: indexPath)
        }
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let imageLoadOperation = imageLoadOperations[indexPath] else { return }
        imageLoadOperation.cancel()
        imageLoadOperations.removeValue(forKey: indexPath)
    }
    
    
}

//extension MainVC: UIViewControllerTransitioningDelegate {
//    
//    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        let photoViewController = presented as! AudioPlayerVC
//        let alphaVariety = "up"
//        animationController.setupImageTransition(data: data!, fromDelegate: self, toDelegate: photoViewController, alphaTabs: alphaVariety)
//        return animationController
//    }
//    
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        let photoViewController = dismissed as! AudioPlayerVC
//        let alphaVariety = "down"
//        animationController.setupImageTransition(data: data!, fromDelegate: photoViewController, toDelegate: self, alphaTabs: alphaVariety)
//        return animationController
//    }
//}

extension MainVC: ImageTransitionProtocol {
    func tranisitionSetup() {}
    
    func tranisitionCleanup() { self.data = nil }
    
    func imageWindowFrame() -> CGRect { return (self.data?.convert)! }
}
