//
//  SignUpVC.swift
//  GysNoize
//
//  Created by iMac_2 on 8/8/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignUpVC: UIViewController, UITextFieldDelegate, UIScrollViewDelegate{
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet weak var emailLogin: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var name: UITextField!
    
    var activeTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        emailLogin.delegate = self
        password.delegate = self
        name.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.unregisterFromKeyboardNotifications()
    }
    
<<<<<<< HEAD
    func registerForKeyboardNotifications() {
        let center:  NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
=======
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func registerForKeyboardNotifications() {
        let center:  NotificationCenter = NotificationCenter.default
        //center.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        //center.addObserver(self, selector: #selector(keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
>>>>>>> c8a5acf7d4e242e881119954941bc5176b380cdd
    }
    
    func unregisterFromKeyboardNotifications () {
        let center:  NotificationCenter = NotificationCenter.default
        center.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        center.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown (notification: NSNotification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let kbSize = ((info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue as CGRect!).size
        
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        var aRect = self.view.frame
        aRect.size.height -= kbSize.height;
        if (!aRect.contains(self.activeTextField.frame.origin) ) {
            self.scrollView.scrollRectToVisible(self.activeTextField.frame, animated: true)
        }
    }
    
    // Called when the UIKeyboardWillHideNotification is sent
    func keyboardWillBeHidden (notification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
    }

    @IBAction func logginPressedEmail(_ sender: Any) {
        if let email = emailLogin.text, let password = password.text, let name = name.text, email.characters.count > 0 && password.characters.count > 0 && name.characters.count > 0 {
            
            loginExistingAccount(email: email, password: password, name: name)
            
        } else {
            self.showError("Username and Password Requared", message: "You must enter both user a name and a password")
        }
    }
    
    func  loginExistingAccount(email: String, password: String, name: String) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if error != nil {
                if let errorCode = AuthErrorCode(rawValue: error!._code) {
                    switch errorCode {
                    case .invalidEmail:
                        self.showError("Warning", message: "Invalid email address")
                    case .wrongPassword:
                        self.showError("Warning", message: "Invalid password")
                    case .emailAlreadyInUse, .accountExistsWithDifferentCredential:
                        self.showError("Warning", message: "Could not create account. Email allready in use")
                    default:
                        self.showError("Warning", message: "There was a problem authenticating. Try again.")
                    }
                }
            } else {
                if user?.uid != nil {
                    DataService.instance.saveUser(uid: user!.uid, name: name, email: email)
                    Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
                        if error != nil {
                            if let errorCode = AuthErrorCode(rawValue: error!._code) {
                                switch errorCode {
                                case .invalidEmail:
                                    self.showError("Warning", message: "Invalid email address")
                                case .wrongPassword:
                                    self.showError("Warning", message: "Invalid password")
                                case .emailAlreadyInUse, .accountExistsWithDifferentCredential:
                                    self.showError("Warning", message: "Could not create account. Email allready in use")
                                default:
                                    self.showError("Warning", message: "There was a problem authenticating. Try again.")
                                }
                            }
                        }else {
                            self.performSegue(withIdentifier: "SignUpVC", sender: nil)
                        }
                    })
                }
            }
        })
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
        //animateViewMoving(up: true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
        //animateViewMoving(up: false, moveValue: 100)
    }
    
//    func animateViewMoving (up:Bool, moveValue :CGFloat){
//        let movementDuration:TimeInterval = 0.3
//        let movement:CGFloat = ( up ? -moveValue : moveValue)
//        UIView.beginAnimations( "animateView", context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        UIView.setAnimationDuration(movementDuration )
//        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
//        UIView.commitAnimations()
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let detail = segue.destination as? MenuController {
//            
//        }
    }

}
