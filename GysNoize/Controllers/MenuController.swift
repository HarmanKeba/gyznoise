//
//  MenuController.swift
//  GysNoize
//
//  Created by iMac_2 on 8/4/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit

class MenuController: UIViewController, UITableViewDataSource {
    
    let cellIdentifiers = ["HOME", "FAVORITES", "RATE US", "FOLLOW US", "SEND DEMO"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        let separatorHeader = UIView(frame: CGRect(x: 0.0, y: tableView.tableHeaderView!.frame.size.height - 1, width: tableView.tableHeaderView!.frame.maxX, height: 1.0))
        //        separatorHeader.backgroundColor = UIColor(white: 224.0/255.0, alpha: 1.0)
        //        tableView.tableHeaderView?.addSubview(separatorHeader)
        //        let separatorFooter = UIView(frame: CGRect(x: 0.0, y: tableView.tableFooterView!.frame.minX + 1, width: tableView.tableFooterView!.frame.maxX, height: 1.0))
        //        separatorFooter.backgroundColor = UIColor(white: 224.0/255.0, alpha: 1.0)
        //        tableView.tableFooterView?.addSubview(separatorFooter)
        
        let selectedView = UIView()
        selectedView.backgroundColor = UIColor.init(red: 0, green: 255, blue: 255, alpha: 1)
//        for cell in staticCells {
//            cell.selectedBackgroundView = selectedView
//        }
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.none
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers[indexPath.row])
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 2:
            iRate.sharedInstance().openRatingsPageInAppStore()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                self.revealViewController().revealToggle(animated: false)
            })
        default: break
        }
    }
}
