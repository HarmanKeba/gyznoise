//
//  LoginExistingAccountVC.swift
//  GysNoize
//
//  Created by iMac_2 on 8/8/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginExistingAccountVC: UIViewController {
    
    @IBOutlet weak var emailLogin: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func logginPressedEmail(_ sender: Any) {
        if let email = emailLogin.text, let password = password.text, email.characters.count > 0 && password.characters.count > 0 {
            
            loginExistingAccount(email: email, password: password)
           
        } else {
            self.showError("Username and Password Requared", message: "You must enter both user a name and a password")
        }
    }
    
    func loginExistingAccount(email: String, password: String) {
        
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            
            if error != nil {
                if let errorCode = AuthErrorCode(rawValue: (error!._code)) {
                    if errorCode == .userNotFound {
                        self.showError("Account not found", message: "You must enter real account data or signIn in bottom buttom")
                        self.emailLogin.text = ""
                        self.password.text = ""
           
                    } else {
                        if let errorCode = AuthErrorCode(rawValue: error!._code) {
                            switch errorCode {
                            case .invalidEmail:
                                self.showError("Warning", message: "Invalid email address")
                            case .wrongPassword:
                                self.showError("Warning", message: "Invalid password")
                            case .emailAlreadyInUse, .accountExistsWithDifferentCredential:
                                self.showError("Warning", message: "Could not create account. Email allready in use")
                            default:
                                self.showError("Warning", message: "There was a problem authenticating. Try again.")
                            }
                        }
                    }
                }
            } else {
                self.performSegue(withIdentifier: "showMains", sender: nil)
            }
        })
    }
    
    @IBAction func signUp(_ sender: Any) {
        self.performSegue(withIdentifier: "signUp", sender: nil)
    }
}
