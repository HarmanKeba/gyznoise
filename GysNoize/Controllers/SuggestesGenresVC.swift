//
//  SuggestesGenresVC.swift
//  GysNoize
//
//  Created by iMac_2 on 8/9/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit

class SuggestesGenresVC: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate let imageLoadQueue = OperationQueue()
    fileprivate var imageLoadOperations = [IndexPath: ImageLoadOperation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 10.0, *) {
            collectionView?.prefetchDataSource = self
            
        }
        
        self.collectionView.decelerationRate = UIScrollViewDecelerationRateFast
        self.collectionView.dataSource = self
        self.collectionView.delegate = self

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestesGenresCell", for: indexPath) as! SuggestesGenresCell
        return cell
    }

}

extension SuggestesGenresVC: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if let _ = imageLoadOperations[indexPath] { return }
            if let viewModel = AudioViewModelController.share.viewModel(at: (indexPath as NSIndexPath).row) {
                let imageLoadOperation = ImageLoadOperation(url: viewModel.images)
                imageLoadQueue.addOperation(imageLoadOperation)
                imageLoadOperations[indexPath] = imageLoadOperation
            }
        }
    }
    
}
