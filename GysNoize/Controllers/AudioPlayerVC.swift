//
//  AudioPlayerVC.swift
//  GysNoize
//
//  Created by iMac_2 on 8/8/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit
import AVFoundation

class AudioPlayerVC: UIViewController {
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var nextTRack: UIButton!
    
    var customView: ImageViewLayer?
    
    @IBOutlet var seekSlider: CustomSlider!
    
    private var playbackLikelyToKeepUpContext = 0
    var playerRateBeforeSeek: Float = 0
    var timeObserver: Any?
    var secondFromStart: Int = 0
    var timer : Timer?
    
    var url: URL?
    
    let avPlayer = AVQueuePlayer()
    var avPlayerLayer: AVPlayerLayer!
    
//    var image: UIImage!
//    var urlAudio: URL?
//    var urlImageWaveFrom: URL?
//    var playerView: ImageViewLayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customView = ImageViewLayer(frame: baseView.bounds)
        baseView.addSubview(customView!)
        
        
        let url = URL(fileURLWithPath: Bundle.main.path(forResource: "test", ofType: "mp3")!)
        let asset = AVAsset(url: url)
        let keys: [String] = ["playable"]
        asset.loadValuesAsynchronously(forKeys: keys) {
            DispatchQueue.main.async {
                self.avPlayer.insert(AVPlayerItem(asset: asset), after: nil)
            }
        }
        let interval = CMTime(seconds: 0.05, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        timeObserver = avPlayer.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { elapsedTime in
            self.observeTime(elapsedTime: elapsedTime)
            self.syncScrubber(elapsedTime: elapsedTime)
            
        })
        avPlayer.addObserver(self, forKeyPath: "currentItem.playbackLikelyToKeepUp",
                             options: .new, context: &playbackLikelyToKeepUpContext)
        
      playBtn.addTarget(self, action: #selector(play), for: .touchUpInside)
        
        let resizeImageMinimun = #imageLiteral(resourceName: "slider").imageWithColor(color: .blue)?.cropImage(toRect: CGRect(x: 0, y: 0, width: #imageLiteral(resourceName: "slider").size.width, height: #imageLiteral(resourceName: "slider").size.height - 100)).resizableImage(withCapInsets: .zero, resizingMode: .tile)
      
        let resizeImageMaximum = #imageLiteral(resourceName: "slider").cropImage(toRect: CGRect(x: 0, y: 0, width: #imageLiteral(resourceName: "slider").size.width, height: #imageLiteral(resourceName: "slider").size.height - 100)).resizableImage(withCapInsets: .zero, resizingMode: .tile)
        seekSlider.setMinimumTrackImage(resizeImageMinimun, for: .normal)
        seekSlider.setMaximumTrackImage(resizeImageMaximum, for: .normal)
        
        seekSlider.setThumbImage(clearImage, for: .normal)
        //baseView.addSubview(seekSlider)
        seekSlider.addTarget(self, action: #selector(sliderBeganTracking), for: .touchDown)
        seekSlider.addTarget(self, action: #selector(sliderEndedTracking), for: [.touchUpInside, .touchUpOutside])
        seekSlider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
    }
    
    var clearImage : UIImage {
        let size = CGSize(width: 1, height: 100)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        let rectanglePath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let color = UIColor.clear
        color.setFill()
        rectanglePath.fill()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        
        //seekSlider.frame = CGRect(x: 0.0, y: UIScreen.main.bounds.maxY - 100.0, width: UIScreen.main.bounds.size.width, height: 100.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        avPlayer.play()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &playbackLikelyToKeepUpContext {
//            if avPlayer.currentItem!.isPlaybackLikelyToKeepUp {
//                
//                
//                let mpic = MPNowPlayingInfoCenter.default()
//                mpic.nowPlayingInfo = [
//                    MPMediaItemPropertyTitle: "\(AudioViewModelController.share.viewModel(at: self.index!.row)!.title)",
//                    MPMediaItemPropertyArtwork : MPMediaItemArtwork(boundsSize: self.imageView!.image!.size, requestHandler: { size -> UIImage in
//                        return self.imageView!.image!
//                    }),
//                    MPMediaItemPropertyPlaybackDuration: CMTimeGetSeconds(self.avPlayer.currentItem!.duration),
//                    MPNowPlayingInfoPropertyElapsedPlaybackTime: 0,
//                    MPNowPlayingInfoPropertyPlaybackRate: 1
//                ]
//            } else {
//            }
//        }
    }
}
    
    private func playerItemDuration() -> CMTime {
        let thePlayerItem = avPlayer.currentItem
        if thePlayerItem?.status == .readyToPlay {
            return thePlayerItem!.duration
        }
        return kCMTimeInvalid
    }
    
    func syncScrubber(elapsedTime: CMTime) {
        let playerDuration = playerItemDuration()
        if CMTIME_IS_INVALID(playerDuration) {
            seekSlider.minimumValue = 0.0
            return
        }
        let duration = Float(CMTimeGetSeconds(playerDuration))
        if duration.isFinite && duration > 0 {
            seekSlider.minimumValue = 0.0
            seekSlider.maximumValue = duration
            let time = Float(CMTimeGetSeconds(elapsedTime))
                seekSlider.setValue(0.0, animated: true)
            
                seekSlider.setValue(time, animated: true)
            
            if seekSlider.value == seekSlider.maximumValue {
                seekSlider.value = 0.0
                avPlayer.seek(to: CMTime(value: CMTimeValue.allZeros, timescale: 1))
            }
        }
    }
    
    func updateTime(_ timer: Timer) {
        let value = Float(CMTimeGetSeconds(avPlayer.currentItem!.duration))
        seekSlider.value = value
        //self.configureMinimumTrackImage(withCoefficient: value)
    }
    
    private func configureMinimumTrackImage(withCoefficient coefficient: Float) {
    
        let image = #imageLiteral(resourceName: "slider").resizableImage(withCapInsets: UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 5.0), resizingMode: .stretch).imageWithColor(color: .blue)
        //self.seekSlider.setMinimumTrackImage(<#T##image: UIImage?##UIImage?#>, for: .normal)
    }
    
    func play() {
       
        if self.avPlayer.rate > 0 {
            self.avPlayer.pause()
           // self.playBtn.setImage(UIImage(named: "play"), for: .normal)
            
        } else {
           
            self.avPlayer.play()
           //self.playBtn.setImage(UIImage(named: "pause"), for: .normal)
        }
    }
    
    
    
    func sliderBeganTracking(slider: UISlider) {
        playerRateBeforeSeek = avPlayer.rate
        avPlayer.pause()
    }
    func sliderEndedTracking(slider: UISlider) {
        let elapsedTime: Float64 = Float64(seekSlider.value)
        updateTimeLabel(elapsedTime: elapsedTime)
        avPlayer.seek(to: CMTimeMakeWithSeconds(elapsedTime, 1000)) { (completed: Bool) -> Void in
            if self.playerRateBeforeSeek > 0 {
                self.avPlayer.play()
            }
        }
    }
    func sliderValueChanged(slider: UISlider) {
        let elapsedTime: Float64 = Float64(seekSlider.value)
        
        updateTimeLabel(elapsedTime: elapsedTime)
    }
    
    
   
    private func observeTime(elapsedTime: CMTime) {
        if secondFromStart >= 30 {
            // if self.devicePasscodeSet() {
//            self.doPause()
//            let delegate = UIApplication.shared.delegate as! AppDelegate
//            delegate.timer?.invalidate()
//            secondFromStart = 0
            // }
        }
        let duration = CMTimeGetSeconds(avPlayer.currentItem!.duration)
        if duration.isFinite {
            let elapsedTime = CMTimeGetSeconds(elapsedTime)
            updateTimeLabel(elapsedTime: elapsedTime)
        }
    }
    
    private func updateTimeLabel(elapsedTime: Float64) {
        let timeRemaining: Float64 = CMTimeGetSeconds(playerItemDuration()) - elapsedTime
        //self.timeRemainingLabel.text = String(format: "%02d:%02d", ((lround(timeRemaining) / 60) % 60), lround(timeRemaining) % 60)
    }


}


class ImageViewLayer: UIImageView {
    var player: AVPlayer? {
        get { return playerLayer.player }
        set { playerLayer.player = newValue }
    }
    
    var playerLayer: AVPlayerLayer { return layer as! AVPlayerLayer }
    
    override class var layerClass: AnyClass { return AVPlayerLayer.self }
}

//extension AudioPlayerVC: ImageTransitionProtocol {
//    func tranisitionSetup() { baseView.isHidden = true }
//    
//    func tranisitionCleanup() { baseView.isHidden = false }
//    
//    //return the imageView window frame
//    func imageWindowFrame() -> CGRect { return baseView.superview!.convert(baseView.frame, to: nil) }
//}

