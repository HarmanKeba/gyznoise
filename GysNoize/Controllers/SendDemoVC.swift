//
//  SendDemoVC.swift
//  GysNoize
//
//  Created by iMac_3 on 07.08.17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import Foundation

import UIKit

class SendDemoVC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        if let url = URL(string: "https://www.label-worx.com/demo/gysnoize-recordings") {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
        
        menuButton.target = revealViewController()
        menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        revealViewController().rearViewRevealWidth = 250
        
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
