//
//  AppDelegate.swift
//  GysNoize
//
//  Created by iMac_2 on 8/2/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import GoogleSignIn
import ReachabilitySwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?

    let reachability = Reachability()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        iRate.sharedInstance().daysUntilPrompt = 20
        iRate.sharedInstance().usesUntilPrompt = 10
        iRate.sharedInstance().remindPeriod = 2
        iRate.sharedInstance().appStoreID = 981778637
        
        return true
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            print("Error to log Google")
            return
        }
        print("Successfully logged in Google")
        guard let idToken = user.authentication.idToken else { return }
        guard  let accessToken = user.authentication.accessToken else { return }
        let credentials = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
        Auth.auth().signIn(with: credentials) { (user, error) in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            guard let userID = user?.uid else { return }
            print(userID)
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
        reachability?.stopNotifier()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
         self.setupNetworkObserving()
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let sourceApplication: String? = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        
        GIDSignIn.sharedInstance().handle(url,
                                          sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                          annotation: [:])
        
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: sourceApplication, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }

    //MARK: - Reachability
    private func setupNetworkObserving() {
        
        reachability?.whenUnreachable = {reachability in
            DispatchQueue.main.async {
                self.window?.rootViewController?.showError("Attention", message: "have some troubles with conection")
            }
        }
        
        do {
            try reachability?.startNotifier()
        } catch {
            //FIXME: - log error
        }
    }

}

