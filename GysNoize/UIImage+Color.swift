//
//  File.swift
//  GysNoize
//
//  Created by iMac_1 on 8/10/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func imageWithColor(color: UIColor) -> UIImage? {
        
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func tintedWithLinearGradientColors(colorsArr: [CGColor]) -> UIImage {
        
        UIGraphicsBeginImageContext(self.size);
        let context = UIGraphicsGetCurrentContext()!
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        
        let colors = colorsArr as CFArray
        let space = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorsSpace: space, colors: colors, locations: [0.3, 0.5])
        
        context.clip(to: rect, mask: self.cgImage!)
        context.drawLinearGradient(gradient!, start: CGPoint(x: 0, y: 0), end: CGPoint(x: self.size.width, y: 0), options: CGGradientDrawingOptions(rawValue: 0))
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return gradientImage!
    }
    
    func cropImage(toRect rect:CGRect) -> UIImage{
        
        let imageRef:CGImage = self.cgImage!.cropping(to: rect)!
        let cropped:UIImage = UIImage(cgImage:imageRef)
        return cropped
    }
    
}
