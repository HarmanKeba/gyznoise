//
//  CustomUISlider.swift
//  GysNoize
//
//  Created by iMac_1 on 8/10/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit

protocol CustomSliderDelegate: class {

    func sliderBeganTracking()
    func sliderEndedTracking()
    func sliderValueChanged()
}

class CustomSlider: UISlider {
    
    weak var delegate: CustomSliderDelegate?
    
    //MARK: - LifeCycle
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 200))
        super.trackRect(forBounds: customBounds)
        
        return customBounds
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
        let rect = self.bounds.insetBy(dx: -10, dy: -10)
        if rect.contains(point){return self}
        return nil
        
    }
    
    override func awakeFromNib() {

        super.awakeFromNib()
        
        self.addTarget(self, action: #selector(CustomSlider.beganTracking), for: .touchDown)
        self.addTarget(self, action: #selector(CustomSlider.endedTracking), for: [.touchUpInside, .touchUpOutside])
        self.addTarget(self, action: #selector(CustomSlider.valueChanged), for: .valueChanged)
    }
    
    //MARK: - Main
    func configureTrack(withImage image: UIImage) {
    
        let resizeImageMinimun = image.imageWithColor(color: .blue)?.cropImage(toRect: CGRect(x: 0, y: 0, width: #imageLiteral(resourceName: "slider").size.width, height: #imageLiteral(resourceName: "slider").size.height - 100)).resizableImage(withCapInsets: .zero, resizingMode: .tile)
        let resizeImageMaximum = image.cropImage(toRect: CGRect(x: 0, y: 0, width: #imageLiteral(resourceName: "slider").size.width, height: #imageLiteral(resourceName: "slider").size.height - 100)).resizableImage(withCapInsets: .zero, resizingMode: .tile)
        
        self.setMinimumTrackImage(resizeImageMinimun, for: .normal)
        self.setMaximumTrackImage(resizeImageMaximum, for: .normal)
        
        self.setThumbImage(self.getClearImage(), for: .normal)
    }
    
    //MARK: - Private 
    private func getClearImage() -> UIImage {
    
        let size = CGSize(width: 1, height: 100)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        let rectanglePath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let color = UIColor.clear
        color.setFill()
        rectanglePath.fill()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    //MARK: - Actions
    func beganTracking() {
    
        self.delegate?.sliderBeganTracking()
    }
    
    func endedTracking() {
        
        self.delegate?.sliderEndedTracking()
    }
    
    func valueChanged() {
        
        self.delegate?.sliderValueChanged()
    }
}
