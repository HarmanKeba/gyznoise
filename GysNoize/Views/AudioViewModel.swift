//
//  AudioViewModel.swift
//  GysNoize
//
//  Created by iMac_2 on 8/3/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import Foundation

struct AudioViewModel {
    var title: String
    var audio: URL
    var images: URL
    var genre: String
    var postKey: String
    var favorites: Bool
    
    init(post: Post) {
        title = post.title
        audio = post.audioURL
        images = post.imagesURL
        postKey = post.postKey
        genre = post.genre
        favorites = post.favorites
    }
}
