//
//  User.swift
//  GysNoize
//
//  Created by iMac_2 on 8/2/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit

struct User {
    private var _firstName: String
    private var _uid: String
    
    var firstName: String {
        return _firstName
    }
    var uid: String {
        return _uid
    }
    
    init(firstName: String, uin: String) {
        _firstName = firstName
        _uid = uin
    }
}

