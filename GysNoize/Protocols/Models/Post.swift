//
//  Post.swift
//  GysNoize
//
//  Created by iMac_2 on 8/3/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import Foundation
import Firebase

struct Post {
    
    private var _title: String
    private var _imagesURL: URL
    private var _postKey: String!
    private var _audioURL: URL
    private var _favorites: Bool
    private var _genre: String
    private var _duration: String
    
    var title: String { return _title }
    var imagesURL: URL { return _imagesURL }
    var audioURL: URL { return _audioURL }
    var postKey: String { return _postKey }
    var favorites: Bool { return _favorites }
    var genre: String { return _genre }
    var duration: String { return _duration }
    
    init(title: String, favorites: Bool, images: URL, audio: URL, postKey: String, genre: String, duration: String) {
        self._title = title
        self._imagesURL = images
        self._audioURL = audio
        self._postKey = postKey
        self._favorites = favorites
        self._genre = genre
        self._duration = duration
    }
}
