//
//  ImageTransitionProtocol.swift
//  GysNoize
//
//  Created by iMac_2 on 8/8/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import Foundation

protocol ImageTransitionProtocol {
    func tranisitionSetup()
    func tranisitionCleanup()
    func imageWindowFrame() -> CGRect
}

