//
//  ImageLoadOperation.swift
//  GysNoize
//
//  Created by iMac_2 on 8/9/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import UIKit
import SDWebImage

typealias ImageLoadOperationCompletionHandlerType = ((UIImage) -> ())?

class ImageLoadOperation: Operation {
    var url: URL
    var completionHandler: ImageLoadOperationCompletionHandlerType
    var image: UIImage?
    
    init(url: URL) { self.url = url }
    
    override func main() {
        if isCancelled { return }
        SDWebImageManager.shared().imageDownloader?.downloadImage(with: url, progress: nil, completed: { (image, error, cache, _) in
            guard !self.isCancelled, let image = image else { return }
            self.image = image
            self.completionHandler?(image)
        })
    }
}
