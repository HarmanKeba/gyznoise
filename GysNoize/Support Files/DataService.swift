//
//  DataService.swift
//  GysNoize
//
//  Created by iMac_2 on 8/2/17.
//  Copyright © 2017 iMac_2. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage

let FIR_CHILD_USER = "user"
let FIR_CHILD_CONTENT = "songs"

class DataService {
    
    static let ds = DataService()
    
    private static let _instance = DataService()
    static var instance: DataService {
        return _instance
    }
    
    var mainRef: DatabaseReference {
        return Database.database().reference(fromURL: "https://gisnoize-69820.firebaseio.com/")
    }
    
    var userRef: DatabaseReference {
        return mainRef.child(FIR_CHILD_USER)
    }
    
    var mediaContentRef: DatabaseReference {
        return mainRef.child(FIR_CHILD_CONTENT)
    }
    
    var mainStorageRef: StorageReference {
        return Storage.storage().reference()
    }
    
    var songsStorage: StorageReference {
        return mainStorageRef.child("songs")
    }
    
    var coverStorage: StorageReference {
        return mainStorageRef.child("covers")
    }
    
    func saveUser(uid: String, name: String, email: String) {
        
        let profile: Dictionary<String, AnyObject> = [
            "name" : name as AnyObject,
            "email" : email as AnyObject
        ]
        
        mainRef.child(FIR_CHILD_USER).child(uid).child("profile").setValue(profile)
    }
    
    func sendMediaPullrequest(senderUID: String, sendingTo: Dictionary<String, User>, mediaURL: URL, textSnippet: String? = nil) {
        
        let pr: Dictionary<String, AnyObject> = [
            "mediaURL" : mediaURL.absoluteString as AnyObject,
            "userID" : senderUID as AnyObject,
            "openCount" : 0 as AnyObject,
            "recipiens" : sendingTo.keys as AnyObject
        ]
        
        mainRef.child("pullRequest").childByAutoId().setValue(pr)
    }
}
